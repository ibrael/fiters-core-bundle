<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 25/10/2016
 * Time: 01:30
 */

namespace Fiters\CoreBundle\EventListener;


use Fiters\CoreBundle\Entity\User;
use Fiters\CoreBundle\Event\UserRegistationSuccessEvent;
use Fiters\CoreBundle\Mailer\UserMailer;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpKernel\Event\KernelEvent;

class UserRegistrationEventListener
{

    /**
     * @var User
     */
    protected $user;

    /**
     * @var UserMailer
     */
    protected $mailer;

    /**
     * UserRegistrationEventListener constructor.
     * @param UserMailer $mailer
     */
    public function __construct(UserMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param GenericEvent $event
     */
    public function onUserInscription(UserRegistationSuccessEvent $event)
    {
        $this->user = $event->getUser();
    }

    public function onKernelTerminate(KernelEvent $event)
    {
        $this->mailer->sendConfirmationEmail($this->user);
    }
}