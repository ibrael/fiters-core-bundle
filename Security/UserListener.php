<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 17/10/2016
 * Time: 22:45
 */

namespace Fiters\CoreBundle\Security;


use Fiters\CoreBundle\Entity\User;
use Fiters\CoreBundle\Mailer\UserMailer;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * Class UserListener
 * @package Fiters\CoreBundle\Security
 */
class UserListener
{

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;


    /**
     * @param User $user
     */
    public function prePersist(User $user)
    {
        if($user->getId() === null){

            $encoder = $this->encoderFactory->getEncoder($user);
            if(!$user->getSalt()) {
                $user->setSalt(hash('sha1', random_bytes(20)));
            }

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());

            $user->setPassword($password);

            $user->setConfirmationToken(uniqid(time(), true));

            $user->setActive(false);

        }
        return $user;
    }

    /**
     * @return EncoderFactoryInterface
     */
    public function getEncoderFactory()
    {
        return $this->encoderFactory;
    }

    /**
     * @param EncoderFactoryInterface $encoderFactory
     * @return UserListener
     */
    public function setEncoderFactory($encoderFactory)
    {
        $this->encoderFactory = $encoderFactory;

        return $this;
    }



}