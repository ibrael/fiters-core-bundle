<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 16/10/2016
 * Time: 14:51
 */

namespace Fiters\CoreBundle\Security;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider extends EntityRepository implements UserLoaderInterface, UserProviderInterface
{
    /**
     * @param string $username
     */
    public function loadUserByUsername($username)
    {
        try {

            return $this->createQueryBuilder('c')
                ->orWhere('c.phone_number = :username')
                ->orWhere('c.email = :username')
                ->setParameter('username', $username)
                ->getQuery()
                ->getSingleResult();

        }
        catch(NoResultException $e) {
            throw new UsernameNotFoundException(
                sprintf('Username "%s" does not exist.', $username)
            );
        }
    }

    public function refreshUser(UserInterface $user)
    {
        $user = $this->loadUserByUsername($user->getUsername());

        return $user;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->getClassName() === $class;
    }
}
