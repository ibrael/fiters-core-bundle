<?php

namespace Fiters\CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('fiters_core');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        $rootNode
            ->children()
                ->arrayNode('templates')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('html')->defaultValue('Emails/registration.html.twig')->end()
                            ->scalarNode('txt')->defaultValue('Emails/registration.txt.twig')->end()
                        ->end()
                    ->end()
                    ->defaultValue([
                        'registration' => [
                            'html' => 'Emails/registration.html.twig',
                            'txt'  => 'Emails/registration.txt.twig'
                        ]
                    ])
                ->end()
                ->arrayNode('mailer')
                    ->children()
                        ->scalarNode('from')->defaultValue('admin@fiters.co')->end()
                        ->scalarNode('from_name')->defaultValue('Webmaster')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
