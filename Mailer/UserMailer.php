<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 24/10/2016
 * Time: 22:05
 */

namespace Fiters\CoreBundle\Mailer;


use Fiters\CoreBundle\Entity\Coach;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

class UserMailer
{
    /**
     * @var EngineInterface
     */
    protected $engine;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var array
     */
    protected $templates = [];

    /**
     * @var array
     */
    protected $config = [];

    /**
     * UserMailer constructor.
     * @param $mailer
     * @param $engine
     * @param $translator
     */
    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $engine,
        TranslatorInterface $translator,
        RouterInterface $router,
        array $templates = [],
        array $config = []
    )
    {
        $this->mailer = $mailer;
        $this->engine = $engine;
        $this->router = $router;
        $this->translator = $translator;
        $this->templates = $templates;
        $this->config = $config;
    }


    /**
     * Send confirmation email
     */
    public function sendConfirmationEmail(UserInterface $user = null)
    {
        if(!$user)
            return;

        $link = $this->router->generate($user instanceof Coach ? 'confirm_coach' : 'confirm_fiter', [
            'confirmation_token' => $user->getConfirmationToken()
        ], RouterInterface::ABSOLUTE_URL);


        $message = \Swift_Message::newInstance()
            ->setSubject($this->translator->trans('Validate your inscription', [], 'FitersCoreBundle'))
            ->setFrom([
                $this->config['from'] => $this->config['from_name']
            ])
            ->setTo([$user->getEmail() => sprintf("%s %s", $user->getFirstname(), $user->getLastname()) ])
            ->setBody(
                $this->engine->render(
                    $this->templates['registration']['html'],
                    array(
                        'user' => $user,
                        'link' => $link
                    )
                ),
                'text/html'
            )

            ->addPart(
                $this->engine->render(
                    $this->templates['registration']['txt'],
                    array(
                        'user' => $user,
                        'link' => $link
                ),
                'text/plain'
            )
            )
        ;

        $this->mailer->send($message);
    }

}