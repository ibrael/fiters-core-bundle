<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 16/10/2016
 * Time: 11:45
 */

namespace Fiters\CoreBundle\Entity;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\EntityListeners;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\MappedSuperclass;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Type;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Bridge\Doctrine\Validator\Constraints as Doctrine;
/**
 * Class User
 * @package AppBundle\Entity
 *
 * @MappedSuperclass()
 * @EntityListeners({"Fiters\CoreBundle\Security\UserListener"})
 * @ExclusionPolicy("all")
 * @ApiDoc(description="User")
 * @Doctrine\UniqueEntity(fields={"email", "phone_number"})
 */
class User implements AdvancedUserInterface
{

    /**
     * @var integer
     * @Id()
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     *
     * @Expose()
     * @Type("integer")
     */
    protected $id;

    /**
     * @var string
     * @Column(type="string", nullable=false, options={"default": "v1.0"})
     * @Expose()
     * @Type("string")
     */
    protected $version = "1.0";

    /**
     * @var string
     * @Column(nullable=true);
     * @Expose()
     * @Type("string")
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @var string
     * @Column(nullable=true)
     * @Expose()
     * @Type("string")
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @var string
     * @Column(nullable=true)
     * @Expose()
     * @Type("string")
     * @Assert\NotBlank()
     */
    protected $phone_prefix_indicator;

    /**
     * @var string
     * @Column(type="string", nullable=false)
     * @Expose()
     * @Type("string")
     * @Assert\NotBlank()
     */
    protected $phone_number;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     * @Expose()
     * @Type("string")
     */
    protected $currency = "EUR";

    /**
     * @var string
     * @Column(type="boolean", nullable=false, options={"default": true })
     * @Expose()
     * @Type("boolean")
     */
    protected $tutorial = true;

    /**
     * @var int
     * @Column(type="integer", nullable=false, options={"default": 0})
     * @Expose()
     * @Type("integer")
     */
    protected $status = 0;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     * @Expose()
     * @Type("string")
     */
    protected $stripeId;

    /**
     * @var string
     * @Column(nullable=true)
     * @Expose()
     * @Type("string")
     *
     * TODO validate this code against codes in database
     */
    protected $parent_code;

    /**
     * @var \DateTime
     * @Column(type="date", nullable=true)
     * @Expose()
     * @Type("DateTime<'U'>")
     */
    protected $birthdate;

    /**
     * @var int
     * @Column(type="integer", nullable=true)
     * @Expose()
     * @Type("integer")
     *
     * @Assert\Choice(choices={0, 1})
     */
    protected $gender;

    /**
     * @var string
     * @Column(type="string")
     * @Expose()
     * @Type("string")
     *
     */
    protected $salt;

    /**
     * @var string
     * @Column(type="string")
     * @Expose()
     * @Type("string")
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="8",
     *     max="50",
     *     minMessage="Password must have at least 8 characters",
     *     maxMessage="Password is too long"
     * )
     */
    protected $password;

    /**
     * @var array
     * @Column(type="array")
     * @Expose()
     * @Type("array")
     */
    protected $roles = [];

    /**
     * @var string
     * @Column(type="boolean")
     * @Expose()
     * @Type("boolean")
     */
    protected $active = false;

    /**
     * @var string
     * @Column(type="boolean")
     * @Expose()
     * @Type("boolean")
     */
    protected $locked = true;


    /**
     * @var string
     * @Column(type="string", unique=true)
     * @Expose()
     * @Type("string")
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     *
     */
    protected $email;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     * @Expose()
     * @Type("string")
     */
    protected $photo_url;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     */
    protected $confirmation_token;


    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->salt = hash('sha512', random_bytes(10));
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photo_url;
    }

    /**
     * @param string $photo_url
     * @return User
     */
    public function setPhotoUrl($photo_url)
    {
        $this->photo_url = $photo_url;

        return $this;
    }



    /**
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @param string $stripeId
     * @return User
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;

        return $this;
    }


    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return $this->active;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return !$this->locked;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return !$this->locked;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     * @return User
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhonePrefixindicator()
    {
        return $this->phone_prefix_indicator;
    }

    /**
     * @param string $phone_prefix_indicator
     * @return User
     */
    public function setPhonePrefixindicator($phone_prefix_indicator)
    {
        $this->phone_prefix_indicator = $phone_prefix_indicator;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param string $phone_number
     * @return User
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return User
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getTutorial()
    {
        return $this->tutorial;
    }

    /**
     * @param string $tutorial
     * @return User
     */
    public function setTutorial($tutorial)
    {
        $this->tutorial = $tutorial;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getParentCode()
    {
        return $this->parent_code;
    }

    /**
     * @param string $parent_code
     * @return User
     */
    public function setParentCode($parent_code)
    {
        $this->parent_code = $parent_code;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param \DateTime $birthdate
     * @return User
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * @return int
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        if(!$this->username)
            $this->username = $email;

        return $this;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param array $roles
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmation_token;
    }

    /**
     * @param string $confirmation_token
     * @return User
     */
    public function setConfirmationToken($confirmation_token)
    {
        $this->confirmation_token = $confirmation_token;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param string $locked
     * @return User
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }



    /**
     * @return bool
     * @Assert\IsTrue(message="Password cannot be equal to your firstname")
     */
    public function isPasswordLegal()
    {
        return $this->firstname !== $this->password;
    }


}
