<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 16/10/2016
 * Time: 11:50
 */

namespace Fiters\CoreBundle\Entity;


use Doctrine\ORM\Mapping\Entity;

/**
 * Class Fiter
 * @package Front\FiterBundle
 *
 * @Entity(repositoryClass="Fiters\CoreBundle\Entity\FiterRepository")
 */
class Fiter extends User
{

    /**
     * Fiter constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles = [ 'ROLE_FITER' ];
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->roles ?: ['ROLE_COACH'];
    }
}
