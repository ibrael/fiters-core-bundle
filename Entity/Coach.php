<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 16/10/2016
 * Time: 11:49
 */


namespace Fiters\CoreBundle\Entity;

use Doctrine\ORM\Mapping\Entity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Class Coach
 * @package Front\CoachBundle\Entity
 *
 * @Entity(repositoryClass="Fiters\CoreBundle\Entity\CoachRepository")
 * @ExclusionPolicy("ALL")
 */
class Coach extends User
{
    /**
     * Coach constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->roles = [ 'ROLE_COACH' ];
    }


    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return $this->roles ?: ['ROLE_COACH'];
    }
}
