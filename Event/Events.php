<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 25/10/2016
 * Time: 01:35
 */

namespace Fiters\CoreBundle\Event;


final class Events
{

    const USER_REGISTRATION_SUCCESS = 'user_registration_success';

    const USER_REGISTRATION_FAILURE = 'user_registration_failure';

    const USER_CONFIRMATION_SUCCESS = 'user_confirmation_success';

    const USER_CONFIRMATION_FAILURE = 'user_confirmation_failure';

}