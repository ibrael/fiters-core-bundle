<?php
/**
 * Created by PhpStorm.
 * User: ibra
 * Date: 25/10/2016
 * Time: 01:37
 */

namespace Fiters\CoreBundle\Event;


use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\User\UserInterface;

class UserRegistationSuccessEvent extends Event
{
    /**
     * @var UserInterface
     */
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return UserRegistationSuccessEvent
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }



}